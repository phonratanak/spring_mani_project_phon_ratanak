package com.maniproject.miniproject.controller;

import com.maniproject.miniproject.message.ResponseFile;
import com.maniproject.miniproject.message.ResponseMessage;
import com.maniproject.miniproject.model.FileDB;
import com.maniproject.miniproject.service.FileStorageService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class FileController {

    @Autowired
    private FileStorageService storageService;



    @PostMapping("/upload")
    public ResponseEntity<HashMap<String,String>> uploadFile(@RequestParam("file") MultipartFile file) {
        HashMap<String,String> show= new HashMap<>();
        try {
            FileDB fileDB= storageService.store(file);
            String fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/files/")
                    .path(fileDB.getId())
                    .toUriString();
            show.put("url",fileDownloadUri);
            return ResponseEntity.status(HttpStatus.OK).body(show);
        } catch (Exception e) {
            show.put("error","401");
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(show);
        }
    }

    @GetMapping("/files")
    public ResponseEntity<List<ResponseFile>> getListFiles() {
        List<ResponseFile> files = storageService.getAllFiles().map(dbFile -> {
            String fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/files/")
                    .path(dbFile.getId())
                    .toUriString();

            return new ResponseFile(
                    dbFile.getName(),
                    fileDownloadUri,
                    dbFile.getType(),
                    dbFile.getData().length);
        }).collect(Collectors.toList());

        return ResponseEntity.status(HttpStatus.OK).body(files);
    }

    @GetMapping("/files/{id}")
    public ResponseEntity<byte[]> getFile(@PathVariable String id) {
        FileDB fileDB = storageService.getFile(id);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileDB.getName() + "\"")
                .body(fileDB.getData());
    }
}
