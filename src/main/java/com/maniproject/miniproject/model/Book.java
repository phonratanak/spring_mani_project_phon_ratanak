package com.maniproject.miniproject.model;

import lombok.*;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "tb_books")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String title;

    private String author;

    private String description;

    private String thumbnail;

    @ManyToOne
    @JoinColumn(name = "category_id" ,referencedColumnName = "id")
    @RestResource(exported = false)
    private Category categories;

    public int getResourceId(){
        return id;
    }
}
