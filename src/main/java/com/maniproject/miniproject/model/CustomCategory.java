package com.maniproject.miniproject.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "customCategory", types = { Category.class })
public interface CustomCategory {
      @Value("#{target.id}")
      Long getId();
}
