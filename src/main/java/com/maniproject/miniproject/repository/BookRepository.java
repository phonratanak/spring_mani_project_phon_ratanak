package com.maniproject.miniproject.repository;

import com.maniproject.miniproject.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.annotation.Resource;
import java.util.List;

@RepositoryRestResource(collectionResourceRel = "books", path = "books")
public interface BookRepository extends JpaRepository<Book,Integer> {
    @RestResource(path="title")
    List<Book> findByTitleIgnoreCaseContaining(@Param("title") String title);
}
