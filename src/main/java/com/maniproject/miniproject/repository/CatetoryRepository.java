package com.maniproject.miniproject.repository;

import com.maniproject.miniproject.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "categories", path = "categories")
public interface CatetoryRepository extends JpaRepository<Category,Long> {
}
