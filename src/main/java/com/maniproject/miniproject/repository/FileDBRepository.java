package com.maniproject.miniproject.repository;

import com.maniproject.miniproject.model.FileDB;
import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
//@RepositoryRestResource(exported = false)
public interface FileDBRepository extends JpaRepository<FileDB,String> {
}
